import ForgeUI, {
  ConfigForm,
  CheckboxGroup,
  Checkbox
} from '@forge/ui';

const Config = () => {
  return (
    <ConfigForm>
      <CheckboxGroup name="myGroup" legend="Select Values">
        {['Value 1', 'Value 2', 'Value 3'].map(value => (
          <Checkbox value={value} label={value}></Checkbox>
        ))}
      </CheckboxGroup>
    </ConfigForm>
  )
};

export default Config;