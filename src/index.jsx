import ForgeUI, { render, Fragment, Macro, Text, useConfig } from "@forge/ui";
import Config from './config';

const App = () => {
  const config = useConfig();

  return (
    <Fragment>
      <Text>{`config.myGroup property is of type ${typeof config.myGroup}`}</Text>
    </Fragment>
  );
};

export const run = render(<Macro app={<App />} config={<Config />} defaultConfig={{myGroup: []}} />);