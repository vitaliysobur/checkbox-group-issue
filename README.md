# Steps to reproduce

- Deploy and install the macro
- Add macro to a page
- Click Edit
- Select multiple checkboxes
- Save and publish the page

# Current Behaviour

Macro text output is `config.myGroup property is of type **string**`

# Expected Behaviour

Macro text output is `config.myGroup property is of type **object**`